package com.pnzgu.restauranapp.controller.oficiant;


import com.pnzgu.restauranapp.dto.ReservDTO;
import com.pnzgu.restauranapp.dto.StolDTO;
import com.pnzgu.restauranapp.service.admin.ReservService;
import com.pnzgu.restauranapp.service.admin.StolService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/oficiant/read")
@RequiredArgsConstructor
public class OficiantController {

    final StolService stolService;
    final ReservService reservService;

    @GetMapping("/readAllStol")
    public ResponseEntity<List<StolDTO>> readAllStol() {
        return new ResponseEntity<>(stolService.getAllStol(), HttpStatus.OK);
    }

    @GetMapping("/readAllReservByStolId/{id}")
    public ResponseEntity<List<ReservDTO>> readAllReservByStolId(@PathVariable Long id) {
        return new ResponseEntity<>(reservService.getAllByStolId(id), HttpStatus.OK);
    }
}
