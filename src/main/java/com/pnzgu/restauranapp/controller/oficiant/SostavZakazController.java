package com.pnzgu.restauranapp.controller.oficiant;

import com.pnzgu.restauranapp.dto.SostavZakazDTO;
import com.pnzgu.restauranapp.service.oficiant.SostavZakazService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/oficiant/sostavZakaz")
@RequiredArgsConstructor
public class SostavZakazController {

    final SostavZakazService sostavZakazService;

    @GetMapping("/readAllSostavByZakazId/{id}")
    public ResponseEntity<List<SostavZakazDTO>> readAllReservByStolId(@PathVariable Long id) {
        return new ResponseEntity<>(sostavZakazService.getAllSostavZakazByZakazId(id), HttpStatus.OK);
    }

    @GetMapping("/read/{id}")
    public ResponseEntity<SostavZakazDTO> read(@PathVariable Long id) {
        return new ResponseEntity<>(sostavZakazService.getSostav(id), HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<SostavZakazDTO> create(@RequestBody SostavZakazDTO sostavZakazDTO) {
        return new ResponseEntity<>(sostavZakazService.save(sostavZakazDTO), HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<SostavZakazDTO> update(@PathVariable Long id, @RequestBody SostavZakazDTO sostavZakazDTO) {
        return new ResponseEntity<>(sostavZakazService.update(id, sostavZakazDTO), HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{id}")
    public HttpStatus delete(@PathVariable Long id) {
        sostavZakazService.delete(id);
        return HttpStatus.OK;
    }
}
