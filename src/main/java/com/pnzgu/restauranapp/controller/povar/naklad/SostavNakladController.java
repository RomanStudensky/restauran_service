package com.pnzgu.restauranapp.controller.povar.naklad;

import com.pnzgu.restauranapp.dto.SostavNakladDTO;
import com.pnzgu.restauranapp.service.povar.SostavNakladService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/povar/sostavNaklad")
@RequiredArgsConstructor
public class SostavNakladController {

    final SostavNakladService sostavNakladService;

    @GetMapping("/readAll")
    public ResponseEntity<List<SostavNakladDTO>> readAll() {
        return new ResponseEntity<>(sostavNakladService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/read/{id}")
    public ResponseEntity<SostavNakladDTO> read(@PathVariable Long id) {
        return new ResponseEntity<>(sostavNakladService.get(id), HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<SostavNakladDTO> create(@RequestBody SostavNakladDTO sostavNakladDTO) {
        return new ResponseEntity<>(sostavNakladService.save(sostavNakladDTO), HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<SostavNakladDTO> update(@PathVariable Long id, @RequestBody SostavNakladDTO sostavNakladDTO) {
        return new ResponseEntity<>(sostavNakladService.update(id, sostavNakladDTO), HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{id}")
    public HttpStatus delete(@PathVariable Long id) {
        sostavNakladService.delete(id);
        return HttpStatus.OK;
    }
}
