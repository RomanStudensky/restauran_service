package com.pnzgu.restauranapp.service.admin;

import com.pnzgu.restauranapp.dto.ReservDTO;
import com.pnzgu.restauranapp.exception.NotFoundException;
import com.pnzgu.restauranapp.store.entity.Reserv;
import com.pnzgu.restauranapp.store.repository.ReservRepository;
import com.pnzgu.restauranapp.util.mapping.SimpleMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ReservService {

    final ReservRepository reservRepository;
    final SimpleMapper<ReservDTO, Reserv> simpleMapper = new SimpleMapper<>(new ReservDTO(), new Reserv());

    public List<ReservDTO> getAllByStolId(Long id) {
        return reservRepository
                .findAllReservByStolId(id)
                .stream()
                .map(simpleMapper::mapEntityToDto)
                .collect(Collectors.toList());
    }

    public ReservDTO get(Long id) {
        return simpleMapper.mapEntityToDto(reservRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Бронь с идентификатором - %s не найден", id))));
    }

    public ReservDTO save(ReservDTO dto) {
        return simpleMapper
                .mapEntityToDto(
                        reservRepository
                                .save(simpleMapper.mapDtoToEntity(dto))
                );
    }

    public ReservDTO update(Long id, ReservDTO dto) {
        reservRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Бронь с идентификатором - %s не найден", id)));

        dto.setId(id);

        return simpleMapper
                .mapEntityToDto(
                        reservRepository.save(simpleMapper.mapDtoToEntity(dto))
                );
    }

    public void delete(Long id) {
        reservRepository.deleteById(id);
    }
}
