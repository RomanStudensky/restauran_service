package com.pnzgu.restauranapp.service.admin;

import com.pnzgu.restauranapp.dto.StolDTO;
import com.pnzgu.restauranapp.exception.NotFoundException;
import com.pnzgu.restauranapp.store.entity.Stol;
import com.pnzgu.restauranapp.store.repository.StolRepository;
import com.pnzgu.restauranapp.util.mapping.SimpleMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StolService {

    final StolRepository stolRepository;
    final SimpleMapper<StolDTO, Stol> simpleMapper = new SimpleMapper<>(new StolDTO(), new Stol());

    public List<StolDTO> getAllStol() {
        return stolRepository
                .findAll()
                .stream()
                .map(simpleMapper::mapEntityToDto)
                .collect(Collectors.toList());
    }

    public StolDTO getStol(Long id) {
        return simpleMapper
                .mapEntityToDto(
                        stolRepository
                                .findById(id)
                                .orElseThrow(() -> new NotFoundException(String.format("Стол с идентификатором - %s не найден", id))));
    }

    public StolDTO save(StolDTO stolDTO) {
        return simpleMapper
                .mapEntityToDto(
                        stolRepository
                                .save(simpleMapper.mapDtoToEntity(stolDTO))
                );
    }

    public StolDTO update(Long id, StolDTO stolDTO) {
        stolRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Стол с идентификатором - %s не найден", id)));

        stolDTO.setId(id);

        return simpleMapper
                .mapEntityToDto(
                        stolRepository.save(simpleMapper.mapDtoToEntity(stolDTO))
                );
    }

    public void delete(Long id) {
        stolRepository.deleteById(id);
    }
}
