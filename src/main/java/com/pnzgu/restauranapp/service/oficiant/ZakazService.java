package com.pnzgu.restauranapp.service.oficiant;

import com.pnzgu.restauranapp.dto.PostavshikDTO;
import com.pnzgu.restauranapp.dto.ZakazDTO;
import com.pnzgu.restauranapp.exception.NotFoundException;
import com.pnzgu.restauranapp.store.entity.Postavshik;
import com.pnzgu.restauranapp.store.entity.Zakaz;
import com.pnzgu.restauranapp.store.repository.ZakazRepository;
import com.pnzgu.restauranapp.util.mapping.SimpleMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ZakazService {

    final ZakazRepository zakazRepository;
    final SimpleMapper<ZakazDTO, Zakaz> simpleMapper = new SimpleMapper<>(new ZakazDTO(), new Zakaz());

    public List<ZakazDTO> getAll() {
        return zakazRepository
                .findAll()
                .stream()
                .map(simpleMapper::mapEntityToDto)
                .collect(Collectors.toList());
    }

    public ZakazDTO get(Long id) {
        return simpleMapper
                .mapEntityToDto(
                        zakazRepository
                                .findById(id)
                                .orElseThrow(() -> new NotFoundException(String.format("Поставщик с идентификатором - %s не найден", id))));

    }

    public ZakazDTO save(ZakazDTO zakazDTO) {
        return simpleMapper
                .mapEntityToDto(
                        zakazRepository
                                .save(simpleMapper.mapDtoToEntity(zakazDTO))
                );
    }

    public ZakazDTO update(Long id, ZakazDTO zakazDTO) {
        zakazRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Поставщик с идентификатором - %s не найден", id)));

        zakazDTO.setId(id);

        return simpleMapper
                .mapEntityToDto(
                        zakazRepository.save(simpleMapper.mapDtoToEntity(zakazDTO))
                );
    }

    public void delete(Long id) {
        zakazRepository.deleteById(id);
    }
}
