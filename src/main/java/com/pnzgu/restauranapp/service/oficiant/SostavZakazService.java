package com.pnzgu.restauranapp.service.oficiant;

import com.pnzgu.restauranapp.dto.DogovorDTO;
import com.pnzgu.restauranapp.dto.SostavZakazDTO;
import com.pnzgu.restauranapp.exception.NotFoundException;
import com.pnzgu.restauranapp.store.entity.Dogovor;
import com.pnzgu.restauranapp.store.entity.SostavZakaz;
import com.pnzgu.restauranapp.store.repository.SostavZakazRepository;
import com.pnzgu.restauranapp.util.mapping.SimpleMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SostavZakazService {

    final SostavZakazRepository sostavZakazRepository;
    final SimpleMapper<SostavZakazDTO, SostavZakaz> simpleMapper = new SimpleMapper<>(new SostavZakazDTO(), new SostavZakaz());

    public List<SostavZakazDTO> getAllSostavZakazByZakazId(Long id) {
        return sostavZakazRepository
                .findAllByZakazId(id)
                .stream()
                .map(simpleMapper::mapEntityToDto)
                .collect(Collectors.toList());
    }

    public SostavZakazDTO getSostav(Long id) {
        return simpleMapper.mapEntityToDto(sostavZakazRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Состава заказа с идентификатором - %s не найден", id))));
    }

    public SostavZakazDTO save(SostavZakazDTO dto) {
        return simpleMapper
                .mapEntityToDto(
                        sostavZakazRepository
                                .save(simpleMapper.mapDtoToEntity(dto))
                );
    }

    public SostavZakazDTO update(Long id, SostavZakazDTO dto) {
        sostavZakazRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Состава заказа с идентификатором - %s не найден", id)));

        dto.setId(id);

        return simpleMapper
                .mapEntityToDto(
                        sostavZakazRepository.save(simpleMapper.mapDtoToEntity(dto))
                );
    }

    public void delete(Long id) {
        sostavZakazRepository.deleteById(id);
    }
}
