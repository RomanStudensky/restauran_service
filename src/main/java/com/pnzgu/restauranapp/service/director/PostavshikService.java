package com.pnzgu.restauranapp.service.director;

import com.pnzgu.restauranapp.dto.PostavshikDTO;
import com.pnzgu.restauranapp.exception.NotFoundException;
import com.pnzgu.restauranapp.store.entity.Postavshik;
import com.pnzgu.restauranapp.store.repository.PostavshikRepository;
import com.pnzgu.restauranapp.util.mapping.SimpleMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PostavshikService {

    final PostavshikRepository postavshikRepository;
    final SimpleMapper<PostavshikDTO, Postavshik> simpleMapper = new SimpleMapper<>(new PostavshikDTO(), new Postavshik());

    public List<PostavshikDTO> getAllPostavshik() {
        return postavshikRepository
                .findAll()
                .stream()
                .map(simpleMapper::mapEntityToDto)
                .collect(Collectors.toList());
    }

    public PostavshikDTO get(Long id) {
        return simpleMapper
                .mapEntityToDto(
                        postavshikRepository
                                .findById(id)
                                .orElseThrow(() -> new NotFoundException(String.format("Поставщик с идентификатором - %s не найден", id))));
    }

    public PostavshikDTO save(PostavshikDTO postavshikDTO) {
        return simpleMapper
                .mapEntityToDto(
                        postavshikRepository
                                .save(simpleMapper.mapDtoToEntity(postavshikDTO))
                );
    }



    public PostavshikDTO update(Long id, PostavshikDTO postavshikDTO) {
        postavshikRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Поставщик с идентификатором - %s не найден", id)));

        postavshikDTO.setId(id);

        return simpleMapper
                .mapEntityToDto(
                        postavshikRepository.save(simpleMapper.mapDtoToEntity(postavshikDTO))
                );
    }

    public void delete(Long id) {
        postavshikRepository.deleteById(id);
    }
}
