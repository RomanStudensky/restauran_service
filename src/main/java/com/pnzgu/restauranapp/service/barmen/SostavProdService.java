package com.pnzgu.restauranapp.service.barmen;

import com.pnzgu.restauranapp.dto.DogovorDTO;
import com.pnzgu.restauranapp.dto.SostavProdDTO;
import com.pnzgu.restauranapp.exception.NotFoundException;
import com.pnzgu.restauranapp.store.entity.Dogovor;
import com.pnzgu.restauranapp.store.entity.SostavProd;
import com.pnzgu.restauranapp.store.repository.SostavProdRepository;
import com.pnzgu.restauranapp.util.mapping.SimpleMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SostavProdService {

    final SostavProdRepository sostavProdRepository;
    final SimpleMapper<SostavProdDTO, SostavProd> simpleMapper = new SimpleMapper<>(new SostavProdDTO(), new SostavProd());


    public List<SostavProdDTO> getAllSostavByProdazaId(Long id) {
        return sostavProdRepository
                .findByProdazaId(id)
                .stream()
                .map(simpleMapper::mapEntityToDto)
                .collect(Collectors.toList());
    }

    public SostavProdDTO getSostav(Long id) {
        return simpleMapper.mapEntityToDto(sostavProdRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Договор с идентификатором - %s не найден", id))));
    }

    public SostavProdDTO save(SostavProdDTO dto) {
        return simpleMapper
                .mapEntityToDto(
                        sostavProdRepository
                                .save(simpleMapper.mapDtoToEntity(dto))
                );
    }

    public SostavProdDTO update(Long id, SostavProdDTO dto) {
        sostavProdRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Договор с идентификатором - %s не найден", id)));

        dto.setId(id);

        return simpleMapper
                .mapEntityToDto(
                        sostavProdRepository.save(simpleMapper.mapDtoToEntity(dto))
                );
    }

    public void delete(Long id) {
        sostavProdRepository.deleteById(id);
    }
}
