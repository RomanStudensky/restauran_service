package com.pnzgu.restauranapp.service.povar;

import com.pnzgu.restauranapp.dto.PostavshikDTO;
import com.pnzgu.restauranapp.dto.SostavOrderDTO;
import com.pnzgu.restauranapp.exception.NotFoundException;
import com.pnzgu.restauranapp.store.entity.Postavshik;
import com.pnzgu.restauranapp.store.entity.SostavOrder;
import com.pnzgu.restauranapp.store.repository.SostavOrderRepository;
import com.pnzgu.restauranapp.store.repository.SostavPostavRepository;
import com.pnzgu.restauranapp.util.mapping.SimpleMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SostavOrderService {

    final SostavOrderRepository sostavPostavRepository;
    final SimpleMapper<SostavOrderDTO, SostavOrder> simpleMapper = new SimpleMapper<>(new SostavOrderDTO(), new SostavOrder());

    public List<SostavOrderDTO> getAllByOrderId(Long id) {
        return sostavPostavRepository
                .findAllByOrdersId(id)
                .stream()
                .map(simpleMapper::mapEntityToDto)
                .collect(Collectors.toList());
    }

    public SostavOrderDTO get(Long id) {
        return simpleMapper.mapEntityToDto(
                sostavPostavRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Состав заявки с идентификатором - %s не найден", id))));
    }

    public SostavOrderDTO save(SostavOrderDTO dto) {
        return simpleMapper
                .mapEntityToDto(
                        sostavPostavRepository
                                .save(simpleMapper.mapDtoToEntity(dto))
                );
    }

    public SostavOrderDTO update(Long id, SostavOrderDTO dto) {
        sostavPostavRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Состав заявки с идентификатором - %s не найден", id)));

        dto.setId(id);

        return simpleMapper
                .mapEntityToDto(
                        sostavPostavRepository.save(simpleMapper.mapDtoToEntity(dto))
                );
    }

    public void delete(Long id) {
        sostavPostavRepository.deleteById(id);
    }
}
