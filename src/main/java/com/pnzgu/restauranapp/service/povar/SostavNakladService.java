package com.pnzgu.restauranapp.service.povar;

import com.pnzgu.restauranapp.dto.SostavNakladDTO;
import com.pnzgu.restauranapp.exception.NotFoundException;
import com.pnzgu.restauranapp.store.entity.SostavNaklad;
import com.pnzgu.restauranapp.store.repository.SostavNakladRepository;
import com.pnzgu.restauranapp.util.mapping.SimpleMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SostavNakladService {

    final SostavNakladRepository sostavNakladRepository;
    final SimpleMapper<SostavNakladDTO, SostavNaklad> simpleMapper = new SimpleMapper<>(new SostavNakladDTO(), new SostavNaklad());

    public List<SostavNakladDTO> getAll() {
        return sostavNakladRepository
                .findAll()
                .stream()
                .map(simpleMapper::mapEntityToDto)
                .collect(Collectors.toList());
    }

    public SostavNakladDTO get(Long id) {
        return simpleMapper
                .mapEntityToDto(
                        sostavNakladRepository
                                .findById(id)
                                .orElseThrow(() -> new NotFoundException(String.format("Поставщик с идентификатором - %s не найден", id))));
    }

    public SostavNakladDTO save(SostavNakladDTO sostavNakladDTO) {
        return simpleMapper
                .mapEntityToDto(
                        sostavNakladRepository
                                .save(simpleMapper.mapDtoToEntity(sostavNakladDTO))
                );
    }



    public SostavNakladDTO update(Long id, SostavNakladDTO sostavNakladDTO) {
        sostavNakladRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Поставщик с идентификатором - %s не найден", id)));

        sostavNakladDTO.setId(id);

        return simpleMapper
                .mapEntityToDto(
                        sostavNakladRepository.save(simpleMapper.mapDtoToEntity(sostavNakladDTO))
                );
    }

    public void delete(Long id) {
        sostavNakladRepository.deleteById(id);
    }

}
