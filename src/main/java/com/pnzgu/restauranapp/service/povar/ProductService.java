package com.pnzgu.restauranapp.service.povar;

import com.pnzgu.restauranapp.dto.PostavshikDTO;
import com.pnzgu.restauranapp.dto.ProductDTO;
import com.pnzgu.restauranapp.exception.NotFoundException;
import com.pnzgu.restauranapp.store.entity.Postavshik;
import com.pnzgu.restauranapp.store.entity.Product;
import com.pnzgu.restauranapp.store.repository.PostavshikRepository;
import com.pnzgu.restauranapp.store.repository.ProductRepository;
import com.pnzgu.restauranapp.util.mapping.SimpleMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductService {

    final ProductRepository postavshikRepository;
    final SimpleMapper<ProductDTO, Product> simpleMapper = new SimpleMapper<>(new ProductDTO(), new Product());

    public List<ProductDTO> getAll() {
        return postavshikRepository
                .findAll()
                .stream()
                .map(simpleMapper::mapEntityToDto)
                .collect(Collectors.toList());
    }

    public ProductDTO get(Long id) {
        return simpleMapper
                .mapEntityToDto(
                        postavshikRepository
                                .findById(id)
                                .orElseThrow(() -> new NotFoundException(String.format("Поставщик с идентификатором - %s не найден", id))));
    }

    public ProductDTO save(ProductDTO dto) {
        return simpleMapper
                .mapEntityToDto(
                        postavshikRepository
                                .save(simpleMapper.mapDtoToEntity(dto))
                );
    }

    public ProductDTO update(Long id, ProductDTO dto) {
        postavshikRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Поставщик с идентификатором - %s не найден", id)));

        dto.setId(id);

        return simpleMapper
                .mapEntityToDto(
                        postavshikRepository.save(simpleMapper.mapDtoToEntity(dto))
                );
    }

    public void delete(Long id) {
        postavshikRepository.deleteById(id);
    }
}
