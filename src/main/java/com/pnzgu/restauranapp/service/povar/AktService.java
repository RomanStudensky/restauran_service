package com.pnzgu.restauranapp.service.povar;

import com.pnzgu.restauranapp.dto.AktDTO;
import com.pnzgu.restauranapp.exception.NotFoundException;
import com.pnzgu.restauranapp.store.entity.AktSpi;
import com.pnzgu.restauranapp.store.repository.AktRepository;
import com.pnzgu.restauranapp.util.mapping.SimpleMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AktService {

    final AktRepository aktRepository;
    final SimpleMapper<AktDTO, AktSpi> simpleMapper = new SimpleMapper<>(new AktDTO(), new AktSpi());

    public List<AktDTO> getAll() {
        return aktRepository
                .findAll()
                .stream()
                .map(simpleMapper::mapEntityToDto)
                .collect(Collectors.toList());
    }

    public AktDTO get(Long id) {
        return simpleMapper
                .mapEntityToDto(
                        aktRepository
                                .findById(id)
                                .orElseThrow(() -> new NotFoundException(String.format("Поставщик с идентификатором - %s не найден", id))));
    }

    public AktDTO save(AktDTO dto) {
        return simpleMapper
                .mapEntityToDto(
                        aktRepository
                                .save(simpleMapper.mapDtoToEntity(dto))
                );
    }

    public AktDTO update(Long id, AktDTO dto) {
        aktRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Поставщик с идентификатором - %s не найден", id)));

        dto.setId(id);

        return simpleMapper
                .mapEntityToDto(
                        aktRepository.save(simpleMapper.mapDtoToEntity(dto))
                );
    }

    public void delete(Long id) {
        aktRepository.deleteById(id);
    }

}
