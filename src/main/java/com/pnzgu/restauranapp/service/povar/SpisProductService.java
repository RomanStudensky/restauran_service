package com.pnzgu.restauranapp.service.povar;

import com.pnzgu.restauranapp.dto.DogovorDTO;
import com.pnzgu.restauranapp.dto.SpisProductDTO;
import com.pnzgu.restauranapp.exception.NotFoundException;
import com.pnzgu.restauranapp.store.entity.Dogovor;
import com.pnzgu.restauranapp.store.entity.SpisProduct;
import com.pnzgu.restauranapp.store.repository.SpisProductRepository;
import com.pnzgu.restauranapp.util.mapping.SimpleMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SpisProductService {

    final SpisProductRepository spisProductRepository;
    final SimpleMapper<SpisProductDTO, SpisProduct> simpleMapper = new SimpleMapper<>(new SpisProductDTO(), new SpisProduct());

    public List<SpisProductDTO> getAllByAktId(Long id) {
        return spisProductRepository
                .findAllByAktId(id)
                .stream()
                .map(simpleMapper::mapEntityToDto)
                .collect(Collectors.toList());
    }

    public SpisProductDTO get(Long id) {
        return simpleMapper.mapEntityToDto(spisProductRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Договор с идентификатором - %s не найден", id))));
    }

    public SpisProductDTO save(SpisProductDTO dto) {
        return simpleMapper
                .mapEntityToDto(
                        spisProductRepository
                                .save(simpleMapper.mapDtoToEntity(dto))
                );
    }

    public SpisProductDTO update(Long id, SpisProductDTO dto) {
        spisProductRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Договор с идентификатором - %s не найден", id)));

        dto.setId(id);

        return simpleMapper
                .mapEntityToDto(
                        spisProductRepository.save(simpleMapper.mapDtoToEntity(dto))
                );
    }

    public void delete(Long id) {
        spisProductRepository.deleteById(id);
    }
}
