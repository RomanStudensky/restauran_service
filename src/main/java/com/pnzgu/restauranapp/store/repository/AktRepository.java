package com.pnzgu.restauranapp.store.repository;

import com.pnzgu.restauranapp.store.entity.AktSpi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AktRepository extends JpaRepository<AktSpi, Long> {
}