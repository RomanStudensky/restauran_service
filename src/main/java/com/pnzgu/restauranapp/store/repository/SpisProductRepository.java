package com.pnzgu.restauranapp.store.repository;

import com.pnzgu.restauranapp.store.entity.SpisProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SpisProductRepository extends JpaRepository<SpisProduct, Long> {
    List<SpisProduct> findAllByAktId(Long id);
}