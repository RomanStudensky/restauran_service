package com.pnzgu.restauranapp.store.repository;

import com.pnzgu.restauranapp.store.entity.SostavOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SostavOrderRepository extends JpaRepository<SostavOrder, Long> {
    List<SostavOrder> findAllByOrdersId(Long id);
}