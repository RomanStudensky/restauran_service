package com.pnzgu.restauranapp.store.repository;

import com.pnzgu.restauranapp.store.entity.SostavNaklad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SostavNakladRepository extends JpaRepository<SostavNaklad, Long> {
}