package com.pnzgu.restauranapp.store.repository;

import com.pnzgu.restauranapp.store.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
}