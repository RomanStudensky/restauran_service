package com.pnzgu.restauranapp.store.repository;

import com.pnzgu.restauranapp.store.entity.Reserv;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReservRepository extends JpaRepository<Reserv, Long> {
    List<Reserv> findAllReservByStolId(Long id);
}