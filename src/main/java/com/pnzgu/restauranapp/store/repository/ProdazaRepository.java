package com.pnzgu.restauranapp.store.repository;

import com.pnzgu.restauranapp.store.entity.Prodaza;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProdazaRepository extends JpaRepository<Prodaza, Long> {
}