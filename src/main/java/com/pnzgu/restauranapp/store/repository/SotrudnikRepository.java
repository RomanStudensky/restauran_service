package com.pnzgu.restauranapp.store.repository;

import com.pnzgu.restauranapp.store.entity.Sotrudnik;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SotrudnikRepository extends JpaRepository<Sotrudnik, Long> {
}