package com.pnzgu.restauranapp.store.repository;

import com.pnzgu.restauranapp.store.entity.Postavshik;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostavshikRepository extends JpaRepository<Postavshik, Long> {
}