package com.pnzgu.restauranapp.store.repository;

import com.pnzgu.restauranapp.store.entity.Dogovor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DogovorRepository extends JpaRepository<Dogovor, Long> {
    Optional<Dogovor> findDogovorsByPostavshikId(Long id);
}