package com.pnzgu.restauranapp.store;

public enum Role {
    DIRECTOR, BUHGALTER, POVAR, ADMIN, OFICIANT, BARMAN
}
