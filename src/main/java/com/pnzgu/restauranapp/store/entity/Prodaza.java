package com.pnzgu.restauranapp.store.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

@Table(name = "prodaza")
@Entity
@Data
public class Prodaza extends EntityParent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_prod", nullable = false)
    private Long id;

    @Column(name = "date_prod", nullable = false)
    private LocalDate dateProd;

    @Column(name = "time_prod", nullable = false)
    private LocalTime timeProd;

    @ManyToOne(optional = false)
    @JoinColumn(name = "sotrud", nullable = false)
    private Sotrudnik sotrud;

    @Column(name = "summa", nullable = false, precision = 131089)
    private BigDecimal summa;

}