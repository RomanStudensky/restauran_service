package com.pnzgu.restauranapp.store.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Table(name = "akt_spis")
@Entity
@Data
public class AktSpi extends EntityParent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_akt", nullable = false)
    private Long id;

    @Column(name = "date_akt", nullable = false)
    private LocalDate dateAkt;

    @ManyToOne(optional = false)
    @JoinColumn(name = "id_sotrud", nullable = false)
    private Sotrudnik sotrud;

    @Column(name = "summa", nullable = false)
    private BigDecimal summa;

}