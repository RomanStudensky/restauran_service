package com.pnzgu.restauranapp.store.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Table(name = "reserv")
@Entity
@Data
public class Reserv extends EntityParent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_reserv", nullable = false)
    private Long id;

    @Column(name = "FIO", nullable = false, length = 50)
    private String fio;

    @Column(name = "date_reserv", nullable = false)
    private LocalDate dateReserv;

    @Column(name = "time_reserv", nullable = false)
    private LocalTime timeReserv;

    @ManyToOne(optional = false)
    @JoinColumn(name = "stol", nullable = false)
    private Stol stol;

    @Column(name = "kolvo_people", nullable = false)
    private Integer kolvoPeople;

}