package com.pnzgu.restauranapp.store.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Table(name = "spis_product")
@Entity
@Data
public class SpisProduct extends EntityParent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_spis", nullable = false)
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "akt", nullable = false)
    private AktSpi akt;

    @ManyToOne(optional = false)
    @JoinColumn(name = "product", nullable = false)
    private Product product;

    @Column(name = "reason", nullable = false, length = 100)
    private String reason;

    @Column(name = "quantity", nullable = false)
    private Double quantity;

    @Column(name = "price", nullable = false, precision = 131089)
    private BigDecimal price;

    @Column(name = "summa", nullable = false, precision = 131089)
    private BigDecimal summa;

}