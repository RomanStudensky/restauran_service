package com.pnzgu.restauranapp.store.entity;

import lombok.Data;

import javax.persistence.*;

@Table(name = "stol")
@Entity
@Data
public class Stol extends EntityParent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_table", nullable = false)
    private Long id;

    @Column(name = "kolvo_place", nullable = false)
    private Integer kolvoPlace;

}