package com.pnzgu.restauranapp.store.entity;

import lombok.Data;

import javax.persistence.*;

@Table(name = "zakaz")
@Entity
@Data
public class Zakaz extends EntityParent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_zakaz", nullable = false)
    private Long id;
}