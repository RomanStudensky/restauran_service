package com.pnzgu.restauranapp;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition
public class RestauranAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestauranAppApplication.class, args);
    }

}
