package com.pnzgu.restauranapp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StolDTO extends DtoParent {
    private Long kolvoPlace;
}
