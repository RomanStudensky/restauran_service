package com.pnzgu.restauranapp.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public abstract class DtoParent {
    private Long id;
}
