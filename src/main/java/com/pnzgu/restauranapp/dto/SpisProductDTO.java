package com.pnzgu.restauranapp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SpisProductDTO extends DtoParent {
    private ProductDTO product;
    private String reason;
    private BigDecimal quantity;
    private BigDecimal price;
    private AktDTO akt;
    private BigDecimal summa;
}
