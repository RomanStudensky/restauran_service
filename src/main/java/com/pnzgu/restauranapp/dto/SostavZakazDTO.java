package com.pnzgu.restauranapp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SostavZakazDTO extends DtoParent {
    private Long bludo;
    private Long quantity;
    private BigDecimal summa;
    private ZakazDTO zakaz;
}