package com.pnzgu.restauranapp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProdazaDTO extends DtoParent { ;
    private LocalDate dateProd;
    private LocalTime timeProd;
    private SotrudnikDTO sotrud;
    private BigDecimal summa;

}
