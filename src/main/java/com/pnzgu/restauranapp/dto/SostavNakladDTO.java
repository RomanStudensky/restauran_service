package com.pnzgu.restauranapp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SostavNakladDTO extends DtoParent {
    private LocalDate dateNak;
    private PostavshikDTO postavshik;
    private DogovorDTO dogovor;
    private BigDecimal summa;

}
