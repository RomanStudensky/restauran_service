package com.pnzgu.restauranapp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AktDTO extends DtoParent {
    private LocalDate dateAkt;
    private SotrudnikDTO sotrud;
    private Double summa;

}
