package com.pnzgu.restauranapp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReservDTO extends DtoParent {
    private String fio;
    private LocalDate dateReserv;
    private LocalTime timeReserv;
    private StolDTO stol;
    private Long kolvoPeople;
}
