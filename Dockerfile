FROM maven:3-openjdk-11
WORKDIR /app
COPY . /app
RUN mvn install -DskipTests

FROM adoptopenjdk:11-jdk
WORKDIR /app
RUN mkdir ./logs
COPY --from=0 /app/target/restauran-app-0.0.1-SNAPSHOT.jar /app/
ENV POSTGRES_PASSWORD=postgres \
        POSTGRES_USER=postgres \
        POSTGRES_DB=rest

CMD java -jar restauran-app-0.0.1-SNAPSHOT.jar
